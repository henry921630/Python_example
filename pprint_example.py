import pprint

dic = {}
for i in range(21):
    dic[i] = "value for %d" % i

# 自定义缩进为4空格
pp = pprint.PrettyPrinter(indent=2)
pp.pprint(dic)
print(pprint.pformat(dic, indent=4))

